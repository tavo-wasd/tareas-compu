Comando para generar rand_i.dat
sh 1-gen_rand.sh i

Comando para generar archivo total
sh 2-gen_total.sh

Comando para ordenar por orden ascendente
( el --numeric-sort : compare according to string numerical value segun el manual)
sort tot_rand.dat --numeric-sort
sort tot_rand.dat --numeric-sort | tac

Comando para ver el tamaño de los archivos
( -a para listar todos )
( -k para block size = 1K )
du -a -k

( -m para block size = 1M )
du -a -m

( --block-size=1G para block size = 1G )
du -a --block-size=1G

Genere una lista de todos los usuarios que se llaman James, cuyo correo electrónico termina en .org y que cooperan con la compañía Ad Astra (1%)
cat usuarios.csv | grep 'James' | grep '\.org' | grep 'Ad Astra'

Genera una lista de todos los usuarios que se llaman James y Paul, pero solo liste aquellos usuarios cuyo apellido empieza con J o S y cooperan con Ad Astra (1%)
cat usuarios.csv | grep 'James\|Paul' | grep '^[A-Za-z]*,J\|^[A-Za-z]*,S' | grep 'Ad Astra'

Cuantos usuarios cuyo nombre empieza con J existen en la base de datos? Describa como determinar esto y escriba los resultados en un archivo auxiliar (1%)
cat usuarios.csv | grep '^J' | wc -l > cantidad_de_J.dat

Para reemplazar todas las instancias de mastercard con Mastercard en vim:
:%s/mastercard/Mastercard/g
