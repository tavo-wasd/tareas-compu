# Bitácora

Cuál sería el comando para ordenar este archivo por orden ascendente
de acuerdo con la columna correspondiente al archivo `rand_i.dat`?
En orden descendente? (0.5%)

```sh
# Ascendente
sort -n rand_total.dat
# Descendente
sort -nr rand_total.dat
```

Cuál sería el comando para determinar el tamaño en kBs de los archivos?
En MBs? GBs? (0.5%)

```sh
# Tamaño en KB
du -ak
# Tamaño en MB
du -am
# Tamaño en GB
du -a --block-size=1G
```

Genere una lista de todos los usuarios que se llaman James, cuyo
correo electrónico termina en .org y que cooperan con la
compañía Ad Astra (1%)

```sh
sed '/^James/!d;/\.org/!d;/Ad Astra/!d' usuarios.csv
```

Genera una lista de todos los usuarios que se llaman James y Paul,
pero solo liste aquellos usuarios cuyo apellido empieza con J o S
y cooperan con Ad Astra (1%)

```sh
sed '/^[JP]a[mu][el]s*,/!d;/^[A-Za-z]*,[JS]/!d;/,Ad Astra$/!d' usuarios.csv
```

Cuántos usuarios cuyo nombre empieza con J existen en la base de datos?
Describa cómo determinar esto y escriba los resultados en un archivo auxiliar (1%)
```sh
grep '^J' usuarios.csv | wc -l > total_con_J.dat
```

Reemplace todas las instancias de mastercard con Mastercard (2%)

```sh
# Solamente ver el resultado
sed 's/mastercard/Mastercard/g' usuarios.csv
# Aplicar cambios
sed 's/mastercard/Mastercard/g' usuarios.csv > usuarios.csv
```
