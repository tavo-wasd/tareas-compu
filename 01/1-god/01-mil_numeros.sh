#!/bin/sh

n="$1"

i=0
while [ "$i" -lt 100 ] ; do
    bash random.sh | grep '^[0-9][0-9]*$'
    i=$((i+1))
done > "rand_$n.dat"
