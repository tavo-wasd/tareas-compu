#include <iostream>
#include <matrix.hpp>

int main() {
	Matrix one(3, 2, 1.0);
	Matrix two(one);
	Matrix three(3, 2, 3.0);

	one.printmat();
	two.printmat();
	three.printmat();

	Matrix four = one + three;
	four.printmat();

	four = one - three;
	four.printmat();

	Matrix five = one - three;
	five.printmat();

	return 0;
}
