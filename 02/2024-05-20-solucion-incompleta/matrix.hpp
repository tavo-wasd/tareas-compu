#ifndef MATRIX_HPP
#define MATRIX_HPP

class Matrix {
public:
	Matrix(int cols, int rows, double value);
	Matrix(const Matrix &obj);
	~Matrix();
	int get_cols();
	int get_rows();
	void printmat();
	Matrix& operator=(const Matrix &obj);
	Matrix operator+(const Matrix &obj);
	Matrix operator-(const Matrix &obj);
private:
	Matrix();
	int n_cols;
	int n_rows;
	double *data;
};

#endif /* MATRIX_HPP */
