#include <iostream>
#include <matrix.hpp>

Matrix::Matrix() {
	n_cols = 0;
	n_rows = 0;
	data = NULL;
}

Matrix::Matrix(const Matrix &obj) {
	n_cols = obj.n_cols;
	n_rows = obj.n_rows;
	data = new double[n_cols*n_rows];

	for (int i = 0; i < n_cols*n_rows; i++)
		data[i] = obj.data[i];
}

Matrix::Matrix(int cols, int rows, double value) {
	n_cols = cols;
	n_rows = rows;

	data = new double[n_cols*n_rows];

	for (int i = 0; i < n_cols*n_rows; i++)
		data[i] = value;
}

void Matrix::printmat() {
	for (int i = 0; i < n_cols; i++) {
		for (int j = 0; j < n_rows; j++) {
			std::cout << data[(i*n_rows)+j] << " " ;
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

Matrix::~Matrix() {
	std::cout << "Destructor called" << std::endl;
	delete[] data;
}

int Matrix::get_cols() {
	return n_cols;
}

int Matrix::get_rows() {
	return n_rows;
}

Matrix& Matrix::operator=(const Matrix &obj) {
	return *this;
}

Matrix Matrix::operator+(const Matrix &obj) {
	Matrix tmp(n_cols, n_rows, 0.0);

	for (int i = 0; i < n_cols*n_rows; i++) {
		tmp.data[i] = data[i] + obj.data[i];
	}

	return tmp;
}

Matrix Matrix::operator-(const Matrix &obj) {
	Matrix tmp(n_cols, n_rows, 0.0);

	for (int i = 0; i < n_cols*n_rows; i++) {
		tmp.data[i] = obj.data[i] - data[i] ;
	}

	return tmp;
}
