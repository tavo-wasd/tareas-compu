No es necesario pero pueden usar el Makefile para compilar el programa
fácilmente, instalen make con `apt-get install make`, tal vez ya lo tienen
instalado.

Cuando lo tengan pueden ejecutar `make` y luego lo pueden correr
escribiendo `./matrix`. Si quieren ayuda para saber cómo
funciona `make` me dicen.

Tip: También pueden correr `make clean` para que les elimine los
archivos objeto y el binario.
